export class UserDto {
  _id: string;
  name: string;
  profession: string;
  age: number;
  email: string;
  password: string;
}
