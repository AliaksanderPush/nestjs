import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { UserDto } from './dto/user.dto';
import { User, UserDocument } from './entities/user.entity';
import { IRole } from './dto/role.dto';
import { Role, RoleDocument } from './entities/role.schema';

@Injectable()
export class UsersRepository {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Role.name) private roleModel: Model<RoleDocument>,
  ) {}

  async find(usersFilterQuery: FilterQuery<User>): Promise<User[]> {
    return await this.userModel.find(usersFilterQuery);
  }

  addUser(user: Omit<UserDto, '_id'>): Promise<User> {
    const newUser = new this.userModel(user);
    return newUser.save();
  }

  addRole(role: { role: string }) {
    const newRole = new this.roleModel(role);
    return newRole.save();
  }

  async searchRole(rol: string): Promise<IRole | null> {
    return await this.roleModel.findOne({ role: rol });
  }

  async searchByAttribute(attr: string): Promise<User | null> {
    return await this.userModel.findOne({ email: attr });
  }

  async findOne(userFilterQuery: string): Promise<User> {
    return this.userModel.findById(userFilterQuery);
  }

  async update(userFilterQuery: string, user: Partial<User>): Promise<User> {
    return this.userModel.findByIdAndUpdate(userFilterQuery, user, {
      new: true,
    });
  }

  async delete(id: string): Promise<void> {
    try {
      await this.userModel.findByIdAndDelete(id);
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
