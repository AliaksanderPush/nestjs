import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { UsersRepository } from './user.repository';
import { User } from './entities/user.entity';
import { hash, compare } from 'bcryptjs';
import { IRole } from './dto/role.dto';

@Injectable()
export class UserService {
  constructor(private readonly usersRepository: UsersRepository) {}

  async findAll(): Promise<User[]> {
    return await this.usersRepository.find({});
  }

  async create(dto: Omit<UserDto, '_id'>) {
    const { name, profession, age, email, password } = dto;
    const hashPass = await hash(password, 7);
    try {
      let userRole = await this.usersRepository.searchRole('user');

      if (!userRole) {
        userRole = await this.usersRepository.addRole({ role: 'user' });
      }

      const newDto = {
        name,
        profession,
        age,
        email,
        password: hashPass,
        roles: [userRole?.role], //userRole?.role,
      };
      return this.usersRepository.addUser(newDto);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async searchByAttributes(email: string) {
    return await this.usersRepository.searchByAttribute(email);
  }

  async findOne(id: string): Promise<User> {
    return await this.usersRepository.findOne(id);
  }

  async update(id: string, dto: Omit<UserDto, '_id'>) {
    return await this.usersRepository.update(id, dto);
  }

  async remove(id: string) {
    await this.usersRepository.delete(id);
  }
}
