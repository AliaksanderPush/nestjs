import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import { User } from './entities/user.entity';
import { Response } from 'express';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return await this.userService.findAll();
  }

  @Post('/register')
  async register(@Body() dto: Omit<UserDto, '_id'>, @Res() res: Response) {
    const condidate = await this.userService.searchByAttributes(dto.email);
    if (!condidate) {
      await this.userService.create(dto);
      res
        .status(HttpStatus.CREATED)
        .json('Пользователь успешно зарегистрирован!');
    } else {
      res.status(401).json('Пользователь с таким email уже зарегистрирован!');
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: Omit<UserDto, '_id'>) {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Res() res: Response) {
    this.userService.remove(id);
    res.status(HttpStatus.OK).json('Пользователь успешно удален!');
  }
}
